package engine

var (
	// 文件上传默认内存缓存大小，默认值是 1 << 32 (32MB)。
	MaxMemory int64 = 32 << 20
)
